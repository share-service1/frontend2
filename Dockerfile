FROM node:12.16.3

WORKDIR /var/www/share_service/

COPY ./package.json /var/www/share_service/package.json


RUN npm install

COPY ./public /var/www/share_service/public
COPY ./src /var/www/share_service/src

RUN npm run build

COPY ./move_build_to_volume.sh /var/www/share_service/move_build_to_volume.sh
