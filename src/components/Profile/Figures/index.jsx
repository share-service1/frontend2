import React from "react";
import './Figures.css'

function figures() {
    return (
        <div className="figures">
            <svg className="rect1" width="214" height="61" viewBox="0 0 214 61" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect opacity="0.4" x="325.019" y="-295" width="34.3083" height="464" transform="rotate(44.3222 325.019 -295)" fill="#5063EE"/>
            </svg>
            <svg className="rect2" width="262" height="171" viewBox="0 0 262 171" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect x="328.736" y="-180" width="20.4484" height="470.501" transform="rotate(44.3222 328.736 -180)" fill="#515360"/>
            </svg>
            <svg className="circle1" width="92" height="50" viewBox="0 0 92 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path opacity="0.21" d="M92 4C92 29.4051 71.4051 50 46 50C20.5949 50 0 29.4051 0 4C0 -21.4051 20.5949 -42 46 -42C71.4051 -42 92 -21.4051 92 4Z" fill="#728CB2"/>
            </svg>
            <svg className="rect3" width="207" height="222" viewBox="0 0 207 222" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect opacity="0.4" x="182.019" width="34.3083" height="464" transform="rotate(44.3222 182.019 0)" fill="#5063EE"/>
            </svg>
            <svg className="rect4" width="153" height="107" viewBox="0 0 153 107" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect opacity="0.4" x="137.735" width="20.4484" height="470.501" transform="rotate(44.3222 137.735 0)" fill="#515360"/>
            </svg>
            <svg className="circle2" width="137" height="137" viewBox="0 0 137 137" fill="none" xmlns="http://www.w3.org/2000/svg">
                <circle opacity="0.21" cx="68.5" cy="68.5" r="68.5" fill="#728CB2"/>
            </svg>
        </div>
    )
}

export default figures