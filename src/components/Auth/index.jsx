import React, {useState} from "react";
import "./Auth.css";
import { authUser } from "./../../store/actions";
import { useDispatch } from "react-redux";

function Auth() {
    const dispatch = useDispatch();
  const [email, getEmail] = useState("");
  const [password, getPassword] = useState("");
  function sendAuthInfo() {
      dispatch(authUser({email, password}))
  }
  return (
    <div className="authModal">
      <div className="authModalContent">
        <input
          type="text"
          className="authInput"
          onChange={(e) => {
            getEmail(e.target.value);
          }}
        />
        <input
          type="password"
          className="authInput"
          onChange={(e) => {
            getPassword(e.target.value);
          }}
        />
        <button className="authBtn" onClick={sendAuthInfo}>Войти</button>
      </div>
    </div>
  );
}

export default Auth;
