import React from 'react';
import {BrowserRouter as Router, Switch, Route, Link} from "react-router-dom";
import './App.css';
import Header from './components/Header';
import MainContent from './components/MainContent';
import Footer from './components/Footer';
import Aside from './components/Aside';
import Auth from './components/Auth';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/auth">
          <Auth />
        </Route>
        <Route  path="/profile">
          <Header />
          <Aside />
          <MainContent />
          <Footer />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
