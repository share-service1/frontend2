import { createAction } from "@reduxjs/toolkit";
import axios from "axios";

axios.defaults.baseURL = `https://mircod.xyz/swagger/?format=openapi`;
axios.interceptors.request.use(function (config) {
  const token = "your_token"; // from store
  config.headers.Authorization = `Token ${token}`;

  return config;
});

export const updateUserInfoSuccess = createAction("UPDATE_USER_INFO_SUCCESS");
export const updateUserAvatarSuccess = createAction("UPDATE_USER_AVATAR_SUCCESS");
export const authUserSuccess = createAction("AUTH_USER_SUCCESS");

export const authUser = (authInfo) => {
  return (dispatch) => {
    axios
      .post("auth/sign_in", authInfo)
      .then((data)=>{
        
        localStorage.setItem('token', data.token);
        dispatch(authUserSuccess(data));
      })
      .catch((err)=>{
        console.log(err);
      });
  };
};

export const updateUserInfo = (userData, setDisplay_saveChanges) => {
  return (dispatch, getState) => {
    let id = getState().userInfo.userInfo.id;
    axios
      .patch("user/"+ {id} +"/", userData)
      .then((data)=>{
        setDisplay_saveChanges(true);
        dispatch(updateUserInfoSuccess(data))})
      .catch((err) => {
        console.log(err);
      });
  };
};

export const postUserAvatar = (userAvatar) => {
  return (dispatch) => {
    axios
      .post("user/avatar/", userAvatar)
      .then((data)=>{
        dispatch(updateUserAvatarSuccess(data))
      })
      .catch((err) => {
        console.log(err);
      });
  }
};

export const postNewPassword = (newPass, hideModal) => {
  return (dispatch, getState) => {
    axios
      .post("auth/set_new_password", getState().userInfo.userInfo.id, newPass)
      .then((data)=>{
        hideModal();
        dispatch(authUserSuccess(data));
      })
      .catch((err) => {
        console.log(err);
      });
  }
}