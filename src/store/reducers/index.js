import { userInfoReducer } from "./userInfoReducer";

export default {
  userInfo: userInfoReducer
};
