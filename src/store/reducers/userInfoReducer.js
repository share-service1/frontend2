import { handleActions } from "redux-actions";
import * as actions from "../actions";

const defaultState = {
  userInfo: {
  },
};

export const userInfoReducer = handleActions(
  {
    [actions.updateUserInfoSuccess](state, { payload }) {
      return {
        ...state,
        userInfo: payload,
      };
    },
    [actions.authUserSuccess](state, {payload}){
      return {
        ...state,
        userInfo: payload,
      };
    },
    [actions.updateUserAvatarSuccess](state, {payload}){
      return {
        ...state,
        userInfo: payload,
      };
    },
  },
  defaultState
);
